document.addEventListener('DOMContentLoaded', function () {
    const openButton = document.getElementById('openPopup');
    const closeButton = document.getElementById('closePopup');
    const popup = document.getElementById('popup');

    openButton.addEventListener('click', function () {
        popup.classList.add('show-popup');
    });

    closeButton.addEventListener('click', function () {
        popup.classList.remove('show-popup');
    });
});


document.addEventListener('DOMContentLoaded', function () {
    var quantityValue = 100; // Valeur initiale
    updateQuantity(); // Appel initial pour mettre à jour la valeur affichée

    document.getElementById('decreaseQuantity').addEventListener('click', function () {
        decreaseQuantity();
    });

    document.getElementById('increaseQuantity').addEventListener('click', function () {
        increaseQuantity();
    });

    function decreaseQuantity() {
        if (quantityValue > 0) {
            quantityValue -= 10; // Ajustez la quantité selon vos besoins
            updateQuantity();
        }
    }

    function increaseQuantity() {
        if (quantityValue < 200) {
        quantityValue += 10; // Ajustez la quantité selon vos besoins
        updateQuantity();
    }
}

    function updateQuantity() {
        var quantityElement = document.getElementById('quantity');
        quantityElement.innerText = quantityValue;
    }
});

// Supposons que votre pop-up ait une classe "popup"
var popup = document.querySelector('.popup');

// Fonction pour afficher le pop-up
function showPopup() {
    popup.classList.add('active');
    document.body.classList.add('popup-active');
}

// Fonction pour masquer le pop-up
function hidePopup() {
    popup.classList.remove('active');
    document.body.classList.remove('popup-active');
}


// Fonction pour appliquer le favori
function applyFavorite(favoriteIcon) {
    const isFavoriteApplied = favoriteIcon.classList.contains('checked');

    // Inverser l'état "checked" pour le favori sélectionné
    if (!isFavoriteApplied) {
        console.log('Favori appliqué');
        favoriteIcon.classList.add('checked');
        favoriteIcon.src = `medias/favicon/favicon-checked.png`;
    } else {
        console.log('Favori désactivé');
        favoriteIcon.classList.remove('checked');
        const uncheckedIconSrc = favoriteIcon.id.includes('unchecked') ? favoriteIcon.src : `medias/favicon/favicon-unchecked.png`;
        favoriteIcon.src = uncheckedIconSrc;
    }
}

// Ajouter un écouteur d'événements clic à chaque élément de favori
document.querySelectorAll('.favorite-icon').forEach(favoriteIcon => {
    favoriteIcon.addEventListener('click', function () {
        applyFavorite(favoriteIcon);
    });
});



//rendre le reste de la page plus sombre lorsque le pop up est ouvert + zoom up
document.addEventListener('DOMContentLoaded', function () {
    const openButton = document.getElementById('openPopup');
    const closeButton = document.getElementById('closePopup');
    const popup = document.getElementById('popup');
    const body = document.body;

    let scrollPosition = 0; // Variable pour sauvegarder la position de la page pour la restaurer a la fermeture du pop up

    openButton.addEventListener('click', function () {
        // Sauvegarder la position actuelle de la page
        scrollPosition = window.scrollY;

        popup.classList.add('show-popup', 'popup-enter');
        body.classList.add('popup-active');
        
        // Appliquer le fond sombreet éviter le défilement de la page
        body.style.overflow = 'hidden';
        body.style.position = 'fixed';
        body.style.top = `-${scrollPosition}px`;
    });

    closeButton.addEventListener('click', function () {
        popup.classList.remove('show-popup', 'popup-enter');
        body.classList.remove('popup-active');
    
        // Retirer le fond sombre et rétablir la position de la page
        body.style.overflow = '';
        body.style.position = '';
        
        // Rétablir la position de la page seulement si elle a été modifiée lors de l'ouverture du pop-up
        if (scrollPosition) {
            window.scrollTo(0, scrollPosition);
            scrollPosition = 0;
        }
    });
});


