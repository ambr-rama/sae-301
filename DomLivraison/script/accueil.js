// Fonction pour appliquer le filtre
function applyFilter(filterElement) {
    const filterType = filterElement.dataset.filterType; // reprend le data filter type initaliser dans chaque div d'un filtre dans le html

    // Vérifier si le filtre est déjà appliqué
    const isFilterApplied = filterElement.classList.contains('checked');

    // Réinitialiser l'état "checked" pour tous les filtres pour s'assurer qu'aucun filtreeees activéau lancement de la page
    document.querySelectorAll('.filter').forEach(filter => {
        filter.classList.remove('checked');
        const filterIcon = filter.querySelector(`#${filterType}Img`);
        if (filterIcon) {
            const uncheckedIconSrc = filterIcon.id.includes('unchecked') ? filterIcon.src : `medias/favicon/${filterType}-unchecked-icon.png`;
            filterIcon.src = uncheckedIconSrc;
        }
    });

    // Si le filtre n'est pas déjà appliqué, l'appliquez
    if (!isFilterApplied) {
        console.log('Filtre appliqué :', filterType);

        // Mettre à jour l'état "checked" pour le filtre sélectionné
        filterElement.classList.add('checked');

        // Mettre à jour le chemin de l'image pour l'état "checked"
        const filterIcon = filterElement.querySelector(`#${filterType}Img`);
        if (filterIcon) {
            filterIcon.src = `medias/favicon/${filterType}-checked-icon.png`;
        }
    } else {
        console.log('Filtre désactivé :', filterType);
    }
}

// Ajouter un écouteur d'événements clic à chaque élément de filtre
document.querySelectorAll('.filter').forEach(filterElement => {
    filterElement.addEventListener('click', function () {
        applyFilter(filterElement);
    });
});



//meme fonctionnement pour le favicon coeur sur chaque produit 

// Fonction pour appliquer le favori
function applyFavorite(favoriteIcon) {
    const isFavoriteApplied = favoriteIcon.classList.contains('checked');

    // Inverser l'état "checked" pour le favori sélectionné
    if (!isFavoriteApplied) {
        console.log('Favori appliqué');
        favoriteIcon.classList.add('checked');
        favoriteIcon.src = `medias/favicon/favicon-checked.png`;
    } else {
        console.log('Favori désactivé');
        favoriteIcon.classList.remove('checked');
        const uncheckedIconSrc = favoriteIcon.id.includes('unchecked') ? favoriteIcon.src : `medias/favicon/favicon-unchecked.png`;
        favoriteIcon.src = uncheckedIconSrc;
    }
}

// Ajouter un écouteur d'événements clic à chaque élément de favori
document.querySelectorAll('.favorite-icon').forEach(favoriteIcon => {
    favoriteIcon.addEventListener('click', function () {
        applyFavorite(favoriteIcon);
    });
});
