<?php 
require(dirname(dirname(__FILE__)).'\modèles\users.php');

function inscription () {
    if(empty($_POST)) {
        require(dirname(dirname(__FILE__)).'\vues\pageInscription.php');
    } else {
        if (empty($_POST['pseudo']) || empty($_POST['mail']) || empty($_POST['pw1']) || empty($_POST['pw2'])) {
            $_POST = [];
            $_SESSION['error'] = "Merci de renseigner tous les champs";
            header("Refresh:0");
        }

        if ($_POST['pw1'] != $_POST['pw2']) {
            $_POST = [];
            $_SESSION['error'] = "Les deux mots de passe ne correspondent pas";
            header("Refresh:0");
        }

        $a = new User(array(
            'nom' => $_POST['pseudo'],
            'passw' => $_POST['pw1'],
            'email' => $_POST['mail']
        ));

        if (existeUtilisateur($a)) {
            $_SESSION['error'] = "Le pseudo ou le mail existe déjà";
            $_POST = [];
            header("Refresh:0");
        }
        // une fois que tout le reste est validé
        // $a = $_POST['login'];

        inscriptionUtilisateur(($a));
        $_SESSION['pseudo'] = $_POST['pseudo'];
        require(dirname(dirname(__FILE__)).'/vues/pageConnexion.php');
    }
}

function connexion () {
    if(empty($_POST)) {
        require(dirname(dirname(__FILE__)).'/vues/pageConnexion.php');
    } else {
        // $a = $_POST['login'];
        $a = new User(array(
            'nom' => $_POST['login'],
            'passw' => $_POST['mdp']
        ));
        if (checkUtilisateur(($a)) == false) {
            $_SESSION['error'] = "login ou mdp invalides";
            $_POST = [];
            header("Refresh:0");
        } else {
            
            $_SESSION['pseudo'] = $_POST['login'];
            header('Location:'.baseURL.'/DomLivraison/index.php');
        }
    }
    
}

function deconnexion() {
    unset($_SESSION['pseudo']);
    header('Location:'.baseURL.'/DomLivraison/index.php');
}
?>