<?php
class Produit{
    //attributs : 
    private $_nom = '';
    private $_id = '';
    private $_prix = '';
    private $_desc = '';
    private $_fav = false;
    //methodes
        //getters
        public function getNom(){
            return($this->_nom);
        }
        public function getID(){
            return($this->_id);
        }
        public function getPrix(){
            return($this->_prix);
        }
        public function getDesc(){
            return($this->_desc);
        }
        public function getFav(){
            return($this->_fav);
        }
        //setters
        public function setNom($n){
            if(is_string($n) && strlen($n)<50){
                $this->_nom = $n;
            }else{
                $type_error = ' | setNom -> la chaîne de caractères est trop longue (max 50) ou contient des caractères interdits';
            }
        }
        public function setID($n){
            if(is_float($n) && strlen($n)<4){
                $this->_id = $n;
            }else{
                $type_error = ' | setID -> l\'ID est invalide';
            }
        }
        public function setPrix($n){
            if(is_float($n) && strlen($n)<7){
                $this->_prix = $n;
            }else{
                $type_error = ' | setPrix -> le prix est invalide';
            }
        }
        public function setDesc($n){
            if(is_string($n) && strlen($n)<550){
                $this->_desc = $n;
            }else{
                $type_error = ' | setDesc -> la chaîne de caractères est trop longue (max 550) ou contient des caractères interdits';
            }
        }
        public function setFav($n){
            if(is_bool($n)){
                $this->_fav = $n;
            }else{
                $type_error = ' | setFav -> la variable rentrée n\'est pas booléenne';
            }
            }
        
        //construct
        public function __construct($a){
            $this->hydrate($a);
        }

        protected function hydrate($array){
            foreach($array as $attr => $value){
                $setter = 'set'.ucfirst($attr);
                if(method_exists($this,$setter)){
                    $this->$setter($value);
        }}}
    }
class Plat extends Produit{
    //attr. additionnels
    private $_filtre = '';
    private $_ajouts = '';
    //méthodes
        //getters
        public function getFiltre(){
            return($this->_filtre);
        }
        public function getAjouts(){
            return($this->_ajouts);
        }
        //setters
        public function setFiltre($f){
            if(is_string($f)){
                $this->_filtre = $f;
            }
        }
        public function setAjouts($a){
            if(is_string($a)){
                $this->_ajouts = $a;
            }
        }
        //autres méthodes
        public function addFiltre($f){
            if(is_string($f)){
                $this->_filtre = $this->_filtre.$f;
            }
        }
        public function addAjouts($a){
            if(is_string($a)){
                $this->_ajouts = $this->_ajouts.$a;
            }
        }
        public function removeAjouts($a){
            if(is_string($a) && str_contains($this->_ajouts, $a)){
                $this->str_replace($a, '', $this->_ajouts);
            }
        }}

class Boisson extends Produit{
    //attr. additionnels
    private $_volume;
       //méthodes
        //getters
        public function getVolume(){
            return($this->_volume);
        }
        //setters
        public function setVolume($v){
            if(is_int($v)){
                $this->_volume = $v;
            }
}}
?>



?>
