<?php
class User{
    //attributs : 
    private $_nom = '';
    private $_id = '';
    private $_email = '';
    private $_passw = '';
    private $_favlist = array();
    //methodes
        //getters
        public function getNom(){
            return($this->_nom);
        }
        public function getID(){
            return($this->_id);
        }
        public function getEmail(){
            return($this->_email);
        }
        public function getPassw(){
            return($this->_passw);
        }
        public function getFavlist(){
            return($this->_favlist);
        }
        //setters
        public function setNom($n){
            if(is_string($n) && strlen($n)<50){
                $this->_nom = $n;
            }else{
                $type_error = ' | username -> la chaîne de caractères est trop longue (max 50) ou contient des caractères interdits';
            }
        }
        public function setID($n){
            if(is_integer($n) && strlen($n)<4){
                $this->_id = $n;
            }else{
                $type_error = ' | setID -> l\'ID est invalide';
            }
        }
        public function setEmail($n){
            if(is_string($n) && strlen($n)<70){
                $this->_email = $n;
            }else{
                $type_error = ' | email -> l\'email est invalide';
            }
        }
        public function setPassw($n){
            if(is_string($n) && strlen($n>8)){
                $this->_passw = $n;
            }else{
                $type_error = ' | password -> la chaîne de caractères est trop courte (min 8)';
            }
        }
        public function setFavlist($n){
            if(is_array($n)){
                $this->_favlist = $n;
            }else{
                $type_error = ' | setFav -> la variable rentrée n\'est pas un tableau';
            }
            }
        
        //construct
        protected function hydrate($array){
            foreach($array as $attr => $value){
                $setter = 'set'.ucfirst($attr);
                if(method_exists($this,$setter)){
                    $this->$setter($value);
        }}}

        public function __construct($a){
            $this->hydrate($a);
        }

        };

?>