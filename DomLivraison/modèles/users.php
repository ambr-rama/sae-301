<?php
require('connect.php');
require(dirname(dirname(__FILE__)).'\classes\User.class.php');
//fonction qui ajoute un utilisateur dans la base
function inscriptionUtilisateur(User $user) {   
    $dbh = connect();
    $sql = "INSERT INTO user (nom, email, passw) VALUES(? ,?, ?)";
    $sth = $dbh->prepare($sql);
    $sth->execute(array($user->getNom(), $user->getEmail(), $user->getPassw()));
    $dbh = null;
}

function existeUtilisateur(User $user) {
    $dbh = connect();
    $sql = "SELECT * FROM user WHERE nom = ? OR email = ?";
    $sth = $dbh->prepare($sql);
    $sth->execute(array($user->getNom(), $user->getEmail()));
    $result = $sth->fetch(PDO::FETCH_ASSOC);    
    $dbh = null;
    if ($result == "") { return false;}
    return(true);
}

function checkUtilisateur(User $user) {
    $dbh = connect();
    $sql = "SELECT * FROM user WHERE nom = ? AND passw = ?";
    $sth = $dbh->prepare($sql);
    $sth->execute(array($user->getNom(), $user->getPassw()));
    $result = $sth->fetch(PDO::FETCH_ASSOC);    
    $dbh = null;    
    if ($result != "") {
        return true; // Le pseudo_utilisateur et le mot de passe correspondent
    } else {
        return false; // Le pseudo_utilisateur n'existe pas ou le mot de passe ne correspond pas
    }}

?>