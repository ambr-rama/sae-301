<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DomLivraison - Produit 1</title>
    <link rel="stylesheet" href="styles/product.css">
    <script defer src="script/product.js"></script>
</head>

<?php
$content = <<<HTML
    <section>
        <div class="product-container">
            <div class="product" id="product1">
                <div class="productImg-container">
                    <img src="medias/product/vegan-bowl-ete.webp" alt="Produit 1">
                </div>
            <h3>Vegan bowl d'été</h3>
            <span class="filter-indicator">Vegan</span>
            <span class="filter-indicator">Promotion</span>
            <p>Dégustez notre délicieux "Vegan Bowl d'Été", une création estivale pleine de saveurs fraîches et végétaliennes. Composé d'une variété d'ingrédients soigneusement sélectionnés, ce bol est une explosion de goûts sains et délicieux. Profitez de la fraîcheur de l'été à chaque bouchée avec notre Vegan Bowl d'Été, disponible dès maintenant.</p>
           
            <a href="#" class="personnaliser-button" id="openPopup">
    <img src="medias/favicon/filter.png" alt="Icone Filtre" class="icon-filter">
    Personnaliser le repas !
</a>

<div id="popup" class="popup">
    <!-- Contenu du popup -->
    <span class="close" id="closePopup">&times;</span>
    
    <h2><img src="medias/favicon/filter.png" alt="Icone Filtre" class="icon-filter">Personnalisation du repas</h2>

    <div class="quantity-container">
    <p>Nombre de lardons en grammes:</p>
        <button id="decreaseQuantity">-</button>
        <span id="quantity">100</span> g <!-- Valeur initiale -->
        <button id="increaseQuantity">+</button>
    </div>
</div>


            <p>Prix : 12€</p>
            <a href="#" class="cta-button">Ajouter au panier</a>
            <img id="favicon-product-1" class="favorite-icon" src="medias/favicon/favicon-unchecked.png" alt="Bouton ajouter au favori">
        </div>
    </div>
</section>



    <section>
    <div class="avis-container">
            <div class="avis">
            <h2>Avis des Clients</h2>
                 <div class="avis-item">
                      <p><strong>Nom Client 1</strong></p>
                      <p>Excellent produit ! J'ai adoré le Vegan Bowl d'Été, une explosion de saveurs.</p>
                </div>

                <div class="avis-item">
                     <p><strong>Nom Client 2</strong></p>
                    <p>Les repas végétaliens sont incroyables. J'ai également apprécié les options sans gluten.</p>
                </div>
             </div>
    </div>
</section>



HTML;
?>

<?php require_once 'template.php'; ?>