<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DomLivraison - Setting </title>
    <link rel="stylesheet" href="styles/setting.css">
    <script defer src="script/setting.js"></script>
</head>

<?php
$connect = <<<HTML
<div class="cta-container">
<a class="cta-link" href="index.php?route=logIn">Se connecter</a>
            <a href="index.php?route=createAccount" class="cta-link">‎ ‎ ‎ ‎ S'inscrire‎ ‎ ‎  ‎  </a>
        </div>
HTML;
$disconnect = <<<HTML
<div class="cta-container">
    <a class="cta-link" href="index.php?route=logOut">Se déconnecter</a>    
</div>
HTML;

if(isset($_SESSION['pseudo'])){
    if($_SESSION['pseudo'] =! ''){
        $content = $disconnect;
    }else{
        $content = $connect;
    }
}else{
    $content = $connect;
}
?>

<?php
$content2 = <<<HTML

<section>

<h2>Votre compte</h2>
        <div class="settings-container">
            <div class="setting">
                <h2>Paramètre 1</h2>
                <p>Description du paramètre 1.</p>
                <input type="text" placeholder="Modifier la valeur">
            </div>

            <div class="setting">
                <h2>Paramètre 2</h2>
                <p>Description du paramètre 2.</p>
                <select>
                    <option value="option1">Option 1</option>
                    <option value="option2">Option 2</option>
                    <option value="option3">Option 3</option>
                </select>
            </div>

            <div class="setting">
                 <h2><a href="faq.php" class="setting-link">FAQ</a></h2>
                <p>Réponse aux questions fréquemment posées.</p>
        </div>
        </div>
    </section>

HTML;
?>


<?php require_once 'template.php'; ?>
