<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DomLivraison - Accueil</title>
    <link rel="stylesheet" href="styles/accueil.css">
    <script defer src="script/accueil.js"></script>
</head>

<?php
$content = <<<HTML
<section class="filter-bar">
    <div class="filter" id="vegan-filter" data-filter-type="vegan">
        <img id="veganImg" src="medias/favicon/vegan-unchecked-icon.png" alt="Icône Vegan" class="filter-icon">
        <div>Vegan</div>
    </div>
    <div class="filter" id="promotion-filter" data-filter-type="promotion">
        <img id="promotionImg" src="medias/favicon/promotion-unchecked-icon.png" alt="Icône Promotion" class="filter-icon">
        <div>Promotion</div>
    </div>
    <div class="filter" id="apperitif-filter" data-filter-type="apperitif">
        <img id="apperitifImg" src="medias/favicon/apperitif-unchecked-icon.png" alt="Icône Apéritifs" class="filter-icon">
        <div>Appéritifs</div>
    </div>
    <div class="filter" id="kasher-filter" data-filter-type="kasher">
        <img id="kasherImg" src="medias/favicon/kasher-unchecked-icon.png" alt="Icône Kasher" class="filter-icon">
        <div>Kasher</div>
    </div>
    <div class="filter" id="halal-filter" data-filter-type="halal">
        <img id="halalImg" src="medias/favicon/halal-unchecked-icon.png" alt="Icône Halal" class="filter-icon">
        <div>Halal</div>
    </div>
</section>

</section>


    <section>
        <h2>Nos Produits</h2>
        <div class="product-container">

            <div class="product" id="product1">
                <a id ="product1" href="index.php?route=products">
                  <img class="productImg" src="medias/product/vegan-bowl-ete.webp" alt="Produit 1">
                </a>
                <h3>Vegan bowl d'été</h3>
                <span class="filter-indicator">Vegan</span>
                <span class="filter-indicator">Promotion</span> 
                <p>Un délicieux bol estival composé de produits frais et végétaliens.</p>
                <p>Prix : 12€</p>
                <a href="#" class="cta-button">Ajouter au panier</a>
                <img id="favicon-product-1" class="favorite-icon" src="medias/favicon/favicon-unchecked.png" alt="Bouton ajouter au favori">
            </div>

            <div class="product" id="product2">
                <img class="productImg" src="medias/product/repas-vegan.webp" alt="Produit 2">
                <h3>Repas vegan du chef</h3>
                <span class="filter-indicator">Vegan</span>
                <p>Un festival de saveurs estivales dans un bol végétalien, un délice rafraîchissant pour les papilles.</p>
                <p>Prix : 22€</p>
                <a href="#" class="cta-button">Ajouter au panier</a>
                <img id="favicon-product-2" class="favorite-icon" src="medias/favicon/favicon-unchecked.png" alt="Bouton ajouter au favori">
            </div>

            <div class="product" id="product3">
                <img class="productImg" src="medias/product/tartiflette-halal.jpg" alt="Produit 1">
                <h3> Tartiflette Halal</h3>
                <span class="filter-indicator">Halal</span>
                <p>Découvrez notre délicieuse Tartiflette Halal, une fusion parfaite de pommes de terre fondantes, de reblochon crémeux et de viande halal savoureuse. Un régal réconfortant pour les amateurs de cuisine halal.</p>
                <p>Prix : 15 €</p>
                <a href="#" class="cta-button">Ajouter au panier</a>
                <img id="favicon-product-3" class="favorite-icon" src="medias/favicon/favicon-unchecked.png" alt="Bouton ajouter au favori">
            </div>

            <div class="product" id="product4">
                <img class="productImg" src="medias/product/boulettes-kasher.jpg" alt="Produit 2">
                <h3>Boulettes Kasher</h3>
                <span class="filter-indicator">Kasher</span>
                <p>Petites merveilles délivrent une explosion de saveurs méditerranéennes dans chaque bouchée.</p>
                <p>Prix : 19 €</p>
                <a href="#" class="cta-button">Ajouter au panier</a>
                <img id="favicon-product-4" class="favorite-icon" src="medias/favicon/favicon-unchecked.png" alt="Bouton ajouter au favori">
            </div>

            <div class="product" id="product5">
                <img class="productImg" src="medias/product/lasagne-italienne.jpg" alt="Produit 1">
                <h3>Lasagne Italienne</h3>
                <span class="filter-indicator">Promotion</span>
                <p>Voyagez au cœur de l'Italie avec nos lasagnes artisanales. Préparées avec passion, chaque couche de pâtes fines, de viande savoureuse et de sauce tomate onctueuse vous transporte dans une expérience culinaire authentique et inoubliable.</p>
                <p>Prix : 12 €</p>
                <a href="#" class="cta-button">Ajouter au panier</a>
                <img id="favicon-product-5" class="favorite-icon" src="medias/favicon/favicon-unchecked.png" alt="Bouton ajouter au favori">
            </div>

            <div class="product" id="product6">
                <img class="productImg" src="medias/product/tartine-avocat.webp" alt="Produit 2">
                <h3>Appéritif : Tartine Avocat Crevettes</h3>
                <span class="filter-indicator">Appéritif</span>
                <p>Découvrez l'harmonie parfaite du crémeux de l'avocat et de la fraîcheur des crevettes dans notre délicieuse tartine. Un mariage exquis de saveurs où la douceur de l'avocat rencontre la délicatesse des crevettes, le tout garni d'une touche citronnée.</p>
                <p>Prix : 6 €</p>
                <a href="#" class="cta-button">Ajouter au panier</a>
                <img id="favicon-product-6" class="favorite-icon" src="medias/favicon/favicon-unchecked.png" alt="Bouton ajouter au favori">
            </div>
            
            
        </div>
    </section>
HTML;
?>

<?php
$content2 = <<<HTML
<div>‎‎</div>
HTML;
?>
<?php require_once 'template.php'; ?>
