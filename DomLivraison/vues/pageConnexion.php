<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DomLivraison - Connexion </title>
    <link rel="stylesheet" href="styles/connexion.css">
    <script defer src="script/connexion.js"></script>
</head>

<?php

$content = <<<HTML
<form action="index.php?route=logIn" method="POST">
<fieldset>
    <legend>Connexion</legend>
        <label for="saisi-log">Login</label>
        <input name="login" size="50" maxlength="50" placeholder="Saisissez votre login" required id="saisi-log">
        <br>
        <label for="saisi-mdp">Mot de passe</label>
        <input name="mdp" size="20" maxlength="20" placeholder="Saisissez votre mot de passe" required id="saisi-mdp">
        <br>
        <button type="submit">Envoyer</button>
    </fieldset>
</form>

HTML;
?>

<?php ob_start();
if(isset($_SESSION['error'])) {
    echo '<p class=error>'.$_SESSION['error'].'</p>';
    unset($_SESSION['error']);
}
?>
<?php
$content2 = ob_get_clean();
require_once 'template.php';
?>