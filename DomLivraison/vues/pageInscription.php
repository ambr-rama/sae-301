<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DomLivraison - Inscription </title>
    <link rel="stylesheet" href="styles/inscription.css">
    <script defer src="script/inscription.js"></script>
</head>

<?php

$content = <<<HTML
<form action="index.php?route=createAccount" method="POST">
    <fieldset>
        <legend>Inscription</legend>
            <label for="saisi-mail">Mail</label>
            <input name="mail" size="30" maxlength="30" type="email" placeholder="Saisissez votre mail" required id="saisi-mail">
            <br>
            <label for="saisi-log">Login</label>
            <input name="pseudo" size="30" maxlength="30" type="text" placeholder="Saisissez votre login" required id="saisi-log">
            <br>
            <label for="saisi-mdp">Mot de passe</label>
            <input name="pw1" size="30" maxlength="30" type="password" placeholder="Saisissez votre mot de passe" required id="saisi-mdp">
            <br>
            <label for="saisi-mdp">Confirmation du mot de passe</label>
            <input name="pw2" size="30" maxlength="30" type="password" placeholder="Saisissez votre mot de passe" required id="saisi-mdp">
            <br>
            <button type="submit">S'inscrire</button>
        </fieldset>
</form>

HTML;
?>

<?php ob_start();
if(isset($_SESSION['error'])) {
    echo '<p class=error>'.$_SESSION['error'].'</p>';
    unset($_SESSION['error']);
}
?>
<?php
$content2 = ob_get_clean();
require_once 'template.php';
?>