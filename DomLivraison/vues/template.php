<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DomLivraison - Site de livraison alimentaire sur Rennes</title>
    <link rel="stylesheet" href="styles/template.css">
</head>

<body>

    <header>
        <div class="logo-container">
        <a href="index.php">
                <img src="medias/logo/domlogo.png" alt="Logo de votre site">
            </a>
        </div>

        <div class="search-bar">
    <input type="text" id="searchInput" placeholder="Rechercher...">
    <button type="button" class="search-button" id="searchButton">
        <img src="medias/favicon/search-icon.png" alt="Logo de recherche">
    </button>
    <button type="button" class="voice-search-button" id="voiceSearchButton">
        <img src="medias/favicon/mic-icon.png" alt="Logo de microphone">
    </button>
</div>



        <div class="icon-container">
        <a href="index.php?route=panier"><img src="medias/favicon/shop-icon.png" alt="Panier"></a>
    <a href="index.php?route=favlist"><img src="medias/favicon/favicon.png" alt="J'aime"></a>
    <a href="index.php?route=settings"><img src="medias/favicon/setting-icon.png" alt="Paramètres"></a>

        </div>
    </header>

    <main>
        <?php echo $content; ?>
    </main>
    
<footer>
    <div class="footer-content">
        <div class="footer-section about">
            <h3>A propos de nous</h3>
            <p>DomLivraison, fondée à Rennes par Dominique Ruffier, est une entreprise dédiée à l'accessibilité de l'alimentation artisanale pour tous. En tant que restaurateur engagé, Dominique met en œuvre son expertise pour garantir une qualité équitable à chaque client, tout en maintenant des prix extrêmement abordables. L'objectif principal de DomLivraison est de rendre les produits alimentaires de qualité accessibles à un large public.</p>
        </div>

        <div class="footer-section links">
            <h3>Liens rapides</h3>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="index.php?route=products">Produits</a></li>
                <li><a href="index.php?route=contact">Contact</a></li>
            </ul>
        </div>

        <div class="footer-section contact">
            <h3>Contactez-nous !</h3>
            <p>Email : contact@domlivraison.com</p>
            <p>Téléphone : 03 81 58 58 67</p>
        </div>
    </div>

    <div class="footer-bottom">
        &copy; 2024 DomLivraison. Tous droits réservés.
    </div>
</footer>

</body>

</html>
