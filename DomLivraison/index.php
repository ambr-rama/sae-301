<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="la dream Team">
    <title>SAÉ 203</title>
</head>
<body>
    <?php
// lancement de la session
    session_start();
// définition de l'URL de base pour les redirections
define('baseURL','http://localhost/sae301/');
// si la route est configurée pour les redirections
// on appelle le bon contrôleur
    if (isset($_GET['route'])) {
        switch( $_GET['route']) {
            case 'panier';
                require('controlleurs\panier.php');

                break;
            case 'favlist';
                require('controlleurs\favlist.php');

                break;
            case 'settings';
                require('controlleurs\settings.php');

                break;
			case 'logIn':
				require('controlleurs\authentification.php');
                connexion();
				break;
			case 'createAccount':
				require('controlleurs\authentification.php');
                inscription();
				break;
            case 'logOut':
                require('controlleurs\authentification.php');
                deconnexion();
                break;
            case 'products':
                require('controlleurs\products.php');
                break;
            // case 'contact'
            //     require('');
            //     break;
        }
    } else {
            // sinon on appelle le contrôleur par défaut
            require('controlleurs\accueil.php');
        }
    ?>
</body>
</html>